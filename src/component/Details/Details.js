import React from 'react';
import axios from 'axios';
import {Link, useParams} from 'react-router-dom';
import editIcon from "../../assets/edit.png"
import deleteIcon from "../../assets/delete.png"

const api =  'http://localhost:3015/api/hostels/';

export default function Details() {
  const [hotel, setHotel] = React.useState([]);

  let {uid} = useParams();

  React.useEffect(() => {
    async function getHotelByUid(uid) {
      if (!uid) {
        return;
      }
      try{
        const newHotel = await axios.get(api + uid);
        setHotel(newHotel.data);
      } catch (e) {
        setHotel(e.message);
      }
    }
    getHotelByUid(uid);
  }, [uid]);

  return <>
    <Link to={"/"}>Retour</Link>
      <table className="table">
        <thead className="thead-light">
        <tr>
          <th scope="col">Nom de la chambre</th>
          <th scope="col">size</th>
          <th scope="col">Editer</th>
          <th scope="col">Supprimer</th>
        </tr>
        </thead>
        <tbody>
        {hotel.roomsData ? hotel.roomsData.map((room, index) => <tr key={index}>
              <td>{room.roomName}</td>
              <td>{room.size}</td>
          <td><Link to={"/hostels/" + uid + "/rooms/" + room.id + "/edit"}><img src={editIcon} alt="roomEdit" className="icon"/></Link></td>
          <td>
                <button className="btn btn-link"><img src={deleteIcon} alt="delete" className="icon"/></button>
          </td>
            </tr>
        ) : null}
        </tbody>
      </table>
  </>
}



