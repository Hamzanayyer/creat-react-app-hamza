import React from 'react';
import axios from 'axios';
import './List.css'
import {Link} from 'react-router-dom';
import loupeIcon from "../../assets/loupe.png"
import editIcon from "../../assets/edit.png"
import deleteIcon from "../../assets/delete.png"
import Modal from '../Modal/Modal';

const api =  'http://localhost:3015/api/hostels/';

export default function List() {
  const [hotels, setHotels] = React.useState([]);
  const [openModal, setOpenModal] = React.useState(false);
  const [id, setId] = React.useState('');

  React.useEffect(() => {
    async function getHotels() {
      try {
        const hotelsToGet = await axios.get(api);
        setHotels(hotelsToGet.data);
      } catch (e) {
        console.log(e.message);
      }
    }
    getHotels();
  }, [openModal]);

  function showModal(id) {
    setOpenModal(true);
    setId(id)
  }

  function closeModal() {
    setOpenModal(false);
  }

  return <>
  <table className="table">
    <thead className="thead-dark">
    <tr>
      <th scope="col">Index</th>
      <th scope="col">Hotel Name</th>
      <th scope="col">Pool</th>
      <th scope="col">Number of room</th>
      <th scope="col">Details</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
    </thead>
    <tbody>
    {hotels.map((hotel, index) => <tr key={index}>
          <td>{index + 1}</td>
          <td>{hotel.name}</td>
          <td>{JSON.stringify(hotel.pool)}</td>
          <td>{hotel.roomNumbers}</td>
          <td><Link to={"/hostels/" + hotel.id}><img src={loupeIcon} alt="loupe" className="icon"/></Link></td>
          <td><Link to={"/hostels/" + hotel.id + "/edit"}><img src={editIcon} alt="edit" className="icon"/></Link></td>
          <td>
            <button className="btn btn-link" onClick={() => showModal(hotel.id)}>
              <img src={deleteIcon} alt="delete" className="icon"/>
            </button>
        </td>
        </tr>
    )}
    </tbody>
  </table>
      <Modal openModal={openModal} setOpenModal={setOpenModal} closeModal={closeModal} hotelId={id}/>
</>
}

