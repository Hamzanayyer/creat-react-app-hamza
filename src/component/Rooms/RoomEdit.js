import React from 'react';
import axios from 'axios';
import {Link, useParams} from 'react-router-dom';

const api = 'http://localhost:3015/api/hostels/';

export default function RoomEdit() {

  const [sending, setSending] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const [state, setState] = React.useState({
    roomName: '',
    size: '',
  });

  let {roomId, uid} = useParams();

  React.useEffect(() => {
    async function editRoom(uid) {
      if (!uid || !sending) {
        return;
      }
      const newRoom = {};
      if (state.name !== '') {
        newRoom.name = state.name;
      }
      if (state.size !== '') {
        newRoom.size = state.size;
      }
      setMessage('...loading');
      try {
        await axios.patch(api + uid, newRoom);
        setMessage('room patched');
      } catch (e) {
        setMessage(e.message);
      }
      setSending(false);
    }
    editRoom(roomId);
  }, [roomId, sending, state.name, state.size]);

  function handleSubmit(event) {
    event.preventDefault();
    setSending(true);
  }

  function handleInput(event) {
    const {id, value, type} = event.target;
    setState({
      ...state,
      [id]: type === 'number' ? parseInt(value) : value,
    });
  }

  return <>
    <p>Veuillez modifier votre hôtel</p>
    <Link to={'/hostels/' + uid}>Retour</Link>
    <div className="container">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="roomName">Nom de la chambre</label>
          <input
              className="form-control"
              value={state.roomName}
              onChange={handleInput}
              type="text" id="roomName"/>
        </div>
        <div className="form-group">
          <label htmlFor="size">Nombre de chambre</label>
          <input
              className="form-control"
              value={state.size}
              onChange={handleInput}
              type="number" id="size"/>
        </div>
        <button type="submit" className="badge badge-dark">Valider</button>
      </form>
      <pre>{message}</pre>
    </div>
  </>;
}
