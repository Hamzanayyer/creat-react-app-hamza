import React from 'react';
import axios from 'axios';

const api =  'http://localhost:3015/api/hostels/';

function Modal({openModal, setOpenModal, closeModal, hotelId}) {
  const [sending, setSending] = React.useState(false);

  React.useEffect(() => {
    async function deleteHotelByUid(hotelId) {
      if (sending === false) {
        return;
      }
      try {
        await axios.delete(api + hotelId);
      } catch (e) {
        console.log(e.message);
      }
      setSending(false);
      setOpenModal(false);
    }
    deleteHotelByUid(hotelId);
  }, [sending, hotelId, setOpenModal]);

  function handleDelete() {
    setSending(true);
  }

    if (openModal === false) {
      return null;
    }

  return (
          <div>
            <div className="modal-header">
              <h5 className="modal-title">Supprimer un hôtel</h5>
            </div>

            <div className="modal-body">
              <p>Etes vous sûr de bien vouloir supprimer l'hôtel ?</p>
            </div>

            <div className="modal-footer">
              <button type="button"
                      className="btn btn-danger"
                      onClick={handleDelete}
              >
                Oui
              </button>
              <button type="button"
                      className="btn btn-secondary"
                      onClick={closeModal}
              >
                Non
              </button>
            </div>
          </div>
  )
}

export default Modal;
