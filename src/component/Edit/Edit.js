import React from 'react';
import axios from 'axios';
import {Link, useParams} from "react-router-dom";

const api = 'http://localhost:3015/api/hostels/';

export default function Edit() {
  const [message, setMessage] = React.useState('');
  const [sending, setSending] = React.useState(false);
  const [state, setState] = React.useState({
    name: '',
    pool: '',
    selectedOption: 'option1',
    roomNumbers: '',
  });

  let {uid} = useParams();

  React.useEffect(() => {
    async function patchHotel(uid) {
      if (!uid || !sending) {
        return;
      }
      const newHotel = {};
      if (state.name !== '') {
        newHotel.name = state.name;
      }
      if (state.roomNumbers !== '') {
        newHotel.roomNumbers = state.roomNumbers;
      }
      if (state.selectedOption === 'option1') {
        newHotel.pool = true;
      }
      if (state.selectedOption === 'option2') {
        newHotel.pool = false;
      }
      setMessage('...loading');
      try {
        await axios.patch(api + uid, newHotel);
        setMessage('hotel patched')
      } catch (e) {
        setMessage(e.message);
      }
      setSending(false)
    }
    patchHotel(uid)
  }, [sending, state.name, state.roomNumbers, state.selectedOption, uid]);

  function handleSubmit(event) {
    event.preventDefault();
    setSending(true)
  }

  function handleInput(event) {
    const {id, value, type} = event.target;
    setState({
      ...state,
      [id]: type === 'number' ? parseInt(value) : value,
    });
  }

  function handleOptionChange(changeEvent) {
    setState({
      ...state,
      selectedOption: changeEvent.target.value,
    });
  }

  return <>
    <p>Veuillez modifier votre hôtel</p>
  <Link to={"/"}>Retour</Link>
  <div className="container">
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="name">Nom de l'hôtel</label>
        <input
            className="form-control"
            value={state.name}
            onChange={handleInput}
            type="text" id="name"/>
      </div>
      <div className="form-group">
        <p>Piscine :</p>
        <label htmlFor="poolTrue">Oui</label>
        <input
            className="form-control"
            value="option1"
            checked={state.selectedOption === 'option1'}
            onChange={handleOptionChange}
            type="radio" id="poolTrue"/>
      </div>
      <div className="form-group">
        <label htmlFor="poolFalse">Non</label>
        <input
            className="form-control"
            value="option2"
            checked={state.selectedOption === 'option2'}
            onChange={handleOptionChange}
            type="radio" id="poolFalse"/>
      </div>
      <div className="form-group">
        <label htmlFor="roomNumbers">Nombre de Chambres</label>
        <input
            className="form-control"
            value={state.roomNumbers}
            onChange={handleInput}
            type="number" id="roomNumbers"/>
      </div>
      <button type="submit" className="badge badge-dark">Valider</button>
    </form>
    <pre>{message}</pre>
  </div>
    </>
}
