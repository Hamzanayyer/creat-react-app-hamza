import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import List from './component/List/List';
import Details from './component/Details/Details';
import Edit from './component/Edit/Edit';
import RoomEdit from './component/Rooms/RoomEdit';

function App() {
  return (
  <BrowserRouter>
    <Switch>
      <Route exact path="/">
        <Redirect
            to={{
              pathname: "/hostels",
            }}
        />
      </Route>
      <Route exact path="/hostels">
        <List/>
      </Route>
      <Route exact path="/hostels/:uid">
        <Details/>
      </Route>
      <Route path="/hostels/:uid/edit">
        <Edit/>
      </Route>
      <Route path="/hostels/:uid/rooms/:roomId/edit">
        <RoomEdit/>
      </Route>
    </Switch>
  </BrowserRouter>
  );
}

export default App;
